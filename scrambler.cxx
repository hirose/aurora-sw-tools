#include<iostream>
#include<vector>
#include<string>

void setbit(unsigned long& value, unsigned int pos, unsigned int val){
  unsigned int value_lsb = (value & 0x00000000FFFFFFFF);
  unsigned int value_msb = (value>>32);
  if(pos>=32){
    value_msb = value_msb & ~(0x0000001<<pos%32);
  }else{
    value_lsb = value_lsb & ~(0x0000001<<pos%32);
  }
  //  std::cout<<"LSB: "<<value_lsb<<std::endl;
  //  std::cout<<"MSB: "<<value_msb<<std::endl;
  value = ((unsigned long)value_lsb) + (((unsigned long)value_msb)<<32) + ((unsigned long)val<<pos);
}

bool getbit(unsigned long value, unsigned int pos){
  unsigned int value_lsb = (value & 0x00000000FFFFFFFF);
  unsigned int value_msb = (value>>32);
  unsigned int ith_word = 0;
  if(pos>=32){
    ith_word = value_msb & (0x0000001<<pos%32);
  }else{
    ith_word = value_lsb & (0x0000001<<pos%32);
  }
  if(ith_word!=0) return true;
  return false;
}

int main(void){

  std::vector<unsigned long> *words = new std::vector<unsigned long>();
  std::vector<unsigned long> *words_msb = new std::vector<unsigned long>();
  std::vector<unsigned long> *output_words = new std::vector<unsigned long>();
  words->push_back(0xf786f30e54f99261);//is written as little endian. (0 bit comes first)
  words->push_back(0x7aec9637fa2a6519);
  words->push_back(0x9430bae5a9a3b611);
  words->push_back(0xafacf8a18efbf875);
  words->push_back(0x34311cb550479f5d);
  words->push_back(0x3a229268cdcf64ad);
  words->push_back(0x66bc4cd4b27e298d);
  words->push_back(0x0303b2000cefb285);
  words->push_back(0x4c03544981ea9eb5);
  words->push_back(0xb6a76cf727adaec1);
  words->push_back(0xbfec35688f28ed51);
  words->push_back(0x50a4750a54263789);
  words->push_back(0x804e577c7b6a198d);
  words->push_back(0xed08289dd6c61221);
  words->push_back(0xc81d4d54f34f52d9);
  words->push_back(0x9ee9d0515d6b9349);
  words->push_back(0xcde8ea8e319da699);

  words_msb->push_back(0x3);//is written as little endian. (0 bit comes first)
  words_msb->push_back(0x1);
  words_msb->push_back(0x0);
  words_msb->push_back(0x2);
  words_msb->push_back(0x0);
  words_msb->push_back(0x1);
  words_msb->push_back(0x2);
  words_msb->push_back(0x1);
  words_msb->push_back(0x3);
  words_msb->push_back(0x1);
  words_msb->push_back(0x3);
  words_msb->push_back(0x1);
  words_msb->push_back(0x3);
  words_msb->push_back(0x3);
  words_msb->push_back(0x2);
  words_msb->push_back(0x3);
  words_msb->push_back(0x3);

  //Need to combine word(0-63 bits) and (64-65bits).
  for(unsigned iword=0; iword<words->size(); iword++){
    words->at(iword) = (words->at(iword)>>2) + (words_msb->at(iword)<<62);
  }

  //  unsigned long test = 0xFFFFFFFFFFFFFFFF;
  unsigned long test = 0x0;
  std::cout<<test<<std::endl;
  //  setbit(test, 0, 0);
  setbit(test, 1, 1);
  std::cout<<test<<std::endl;

//   unsigned long test = 0xF000000000000000;
//   bool ret = getbit(test, 0);
//   std::cout<<(ret?"1":"0")<<std::endl;

//First two bits added (in total 66 bits)
//  words->push_back(0x3f786f30e54f99261);
//  words->push_back(0x17aec9637fa2a6519);
//  words->push_back(0x09430bae5a9a3b611);
//  words->push_back(0x2afacf8a18efbf875);
//  words->push_back(0x034311cb550479f5d);
//  words->push_back(0x13a229268cdcf64ad);
//  words->push_back(0x266bc4cd4b27e298d);
//  words->push_back(0x10303b2000cefb285);
//  words->push_back(0x34c03544981ea9eb5);
//  words->push_back(0x1b6a76cf727adaec1);
//  words->push_back(0x3bfec35688f28ed51);
//  words->push_back(0x150a4750a54263789);
//  words->push_back(0x3804e577c7b6a198d);
//  words->push_back(0x3ed08289dd6c61221);
//  words->push_back(0x2c81d4d54f34f52d9);
//  words->push_back(0x39ee9d0515d6b9349);
//  words->push_back(0x3cde8ea8e319da699);
//
  unsigned long word_in_ff=0;
  for(unsigned iword=0; iword<words->size(); iword++){
    std::cout<<"word["<<iword<<"]: "<<std::hex<<words->at(iword)<<std::dec<<std::endl;

    //Initialization for (de-)scrambling...
    //    if(iword==0) word_in_ff=0x0000000000000001;
    //    if(iword==0) word_in_ff=0xFFFFFFFFFFFFFFFF;
    if(iword==0) word_in_ff=0x0000000000000000;
    unsigned long cur_word=words->at(iword);
    unsigned long out_word=0;

    //Actual de-scrambling...
    for(int icnt=0; icnt<64; icnt++){
      //Making output by scrambler
      //      bool curbit = getbit(cur_word,63-icnt);//Note endianness
      //      bool curbit = getbit(cur_word,63-icnt);//Note endianness
      bool curbit = getbit(cur_word,icnt);//Note endianness
      bool outbit = (getbit(word_in_ff,38) ^ getbit(word_in_ff,57)) ^ curbit;
      //      std::cout<<"Output["<<icnt<<"]="<<outbit<<std::endl;
      setbit(out_word,63-icnt,outbit);//Note endianness
      word_in_ff=word_in_ff<<1;
      setbit(word_in_ff,0,curbit);//de-scrambler
      //      setbit(word_in_ff,0,outbit);//scrambler

      std::cout<<"curbit="<<curbit<<std::endl;
      std::cout<<"outbit="<<outbit<<std::endl;
      std::cout<<"word_in_ff=================="<<std::endl;
      for(int icnt=0; icnt<64; icnt++){
        bool ret = getbit(word_in_ff,icnt);
        if     (icnt%8==0) std::cout<<icnt<<": "<<(ret?"1":"0");
        else if(icnt%8==7) std::cout<<(ret?"1":"0")<<std::endl;
        else               std::cout<<(ret?"1":"0");
      }

    }

//     //Debug output
//     std::cout<<"output word: "<<std::hex<<out_word<<std::dec<<std::endl;
//     for(int icnt=0; icnt<64; icnt++){
//       bool ret = getbit(out_word,icnt);
//       if     (icnt%8==0) std::cout<<icnt<<": "<<(ret?"1":"0");
//       else if(icnt%8==7) std::cout<<(ret?"1":"0")<<std::endl;
//       else               std::cout<<(ret?"1":"0");
//     }

    output_words->push_back(out_word);
  }


  for(unsigned iword=0; iword<output_words->size(); iword++){
    std::cout<<"       words["<<iword<<"]: "<<std::hex<<words->at(iword)<<std::dec<<std::endl;
    std::cout<<"output_words["<<iword<<"]: "<<std::hex<<output_words->at(iword)<<std::dec<<std::endl;

    //Initialization for (de-)scrambling...
    //    if(iword==0) word_in_ff=0x0000000000000001;
    //    if(iword==0) word_in_ff=0xFFFFFFFFFFFFFFFF;
    if(iword==0) word_in_ff=0x0000000000000000;
    unsigned long cur_word=output_words->at(iword);
    unsigned long out_word=0;

    //Actual de-scrambling...
    for(int icnt=0; icnt<64; icnt++){
      //Making output by scrambler
      //      bool curbit = getbit(cur_word,63-icnt);//Note endianness
      bool curbit = getbit(cur_word,63-icnt);//Note endianness
      bool outbit = (getbit(word_in_ff,38) ^ getbit(word_in_ff,57)) ^ curbit;
      //      std::cout<<"Output["<<icnt<<"]="<<outbit<<std::endl;
      setbit(out_word,63-icnt,outbit);//Note endianness
      word_in_ff=word_in_ff<<1;
      //      setbit(word_in_ff,0,curbit);//de-scrambler
      setbit(word_in_ff,0,outbit);//scrambler

      //      std::cout<<"curbit="<<curbit<<std::endl;
      //      std::cout<<"outbit="<<outbit<<std::endl;
      //      std::cout<<"word_in_ff=================="<<std::endl;
//       for(int icnt=0; icnt<64; icnt++){
//         bool ret = getbit(word_in_ff,icnt);
//         if     (icnt%8==0) std::cout<<icnt<<": "<<(ret?"1":"0");
//         else if(icnt%8==7) std::cout<<(ret?"1":"0")<<std::endl;
//         else               std::cout<<(ret?"1":"0");
//       }
    }
    std::cout<<"                 "<<std::hex<<out_word<<std::dec<<std::endl;
  }

  return 0;

}
